export let Links = [
    { name: "Homepage", link: "/" },
    {
      name: "Bobosoho Shop",
      link: "/",
      submenu: [
        { name: "Category 1", link: "/category1" },
        { name: "Category 2", link: "/category2" },
      ],
    },
    {
      name: "Soho Server Hosting",
      link: "/",
      submenu: [
        { name: "Hosting Plan 1", link: "/hosting-plan1" },
        { name: "Hosting Plan 2", link: "/hosting-plan2" },
      ],
    },
    { name: "Free Products", link: "/" },
    { name: "Contact Us", link: "/" },
    {
      name: "FAQ",
      link: "/",
      submenu: [
        { name: "FAQ Section 1", link: "/faq-section1" },
        { name: "FAQ Section 2", link: "/faq-section2" },
      ],
    },
  ];