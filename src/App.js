import Home from "./Component/HomePage/Home/Home";
import Navbar from "./Component/Navigation/Navbar";

function App() {
  return (
    <>
      <Navbar />
      <Home />
    </>
  );
}

export default App;
